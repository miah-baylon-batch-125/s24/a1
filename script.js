db.users.insertMany(
   [ {
      "firstName": "Diane",
      "lastName": "Murphy",
      "email": "dmurphy@mail.com",
      "isAdmin": false,
      "isActive": true
   },
   {
      "firstName": "Mary",
      "lastName": "Patterson",
      "email": "mpatterson@mail.com",
      "isAdmin": false,
      "isActive": true
   },
   {
      "firstName": "Jeff",
      "lastName": "Firrelli",
      "email": "jfirrelli@mail.com",
      "isAdmin": false,
      "isActive": true
   },
   {
      "firstName": "Gerard",
      "lastName": "Bondur",
      "email": "gbondur@mail.com",
      "isAdmin": false,
      "isActive": true
   },
   {
      "firstName": "Pamela",
      "lastName": "Castillo",
      "email": "pcastillo@mail.com",
      "isAdmin": true,
      "isActive": false
   },
   {
      "firstName": "George",
      "lastName": "Vanauf",
      "email": "gvanauf@mail.com",
      "isAdmin": true,
      "isActive": true
   }
   ]);


db.courses.insertMany(
   [ {
      "name": "Professional Development",
      "price": "10,000.00"
   },
   {
      "name": "Business Processing",
      "price": "13,000.00"
   }
 
   ]);


db.courses.updateOne( 
      {
          "name": "Professional Development"
      },
      {
         $set: { "enrollees": [ObjectId("6125b47746233726d4c363d6"), ObjectId("6125b47746233726d4c363d8")]}
      }
      );

db.courses.updateOne( 
      {
          "name": "Business Processing"
      },
      {
         $set: { "enrollees": [ObjectId("6125b47746233726d4c363d9"), ObjectId("6125b47746233726d4c363d7")]}
      }
      );


  db.users.find(
   {
      "isAdmin": false
   }
   );
